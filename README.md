# TheFallOfTheHighGardensOfAkkadia
_Beat'em up_ type game in C#, using Unity engine 

--ISSUES--
- enemy respawn
- types of enemies: create more enemy types
- attack types: create different damage types; combos; ...
- camera script: when jumping, player and camera rotation aren't aligned
- player and enemy sound: add sound 
- slow walk under waterfalls: add 
- transition from walking/running to falling: add animation
